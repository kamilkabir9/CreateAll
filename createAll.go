package CreateAll

import (
	"os"
	"path/filepath"
)

//Function CreateAll() creates "a/b/c/MyFile.txt" at a/b/c and returns MyFile.txt
func CreateAll(path string, perm os.FileMode) (*os.File, error) {
	err := os.MkdirAll(filepath.Dir(path), perm)
	if err != nil {
		return nil, err
	}
	return os.Create(path)
}
